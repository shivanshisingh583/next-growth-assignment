// PAGE2-JS

document.addEventListener('DOMContentLoaded', () => {
    const productsContainer = document.querySelector('.products');
      const loader = document.getElementById('loader');
    let currentPage = 1; 
    let isLoading = false; 

    async function fetchAndDisplayProducts(url) {
        isLoading = true;
           loader.style.display = 'block';

        try {
            const response = await fetch(`${url}?page=${currentPage}`);
            const data = await response.json();
            const fragment = document.createDocumentFragment();

            data.forEach(product => {
                const { title, description, image, category, price, id } = product;

                const productDiv = document.createElement('div');
                productDiv.classList.add('product');

                productDiv.innerHTML = `
                    <img src="${image}" alt="${category}" class="product-img">
                    <div class="product-content">
                        <h2 class="product-title">${title.length > 18 ? title.substring(0, 18).concat(' ...') : title}</h2>
                        <h4 class="product-category">${category}</h4>
                        <p class="product-description">${description.length > 80 ? description.substring(0, 80).concat(' ...more') : description}</p>
                        <div class="product-price-container">
                            <h3 class="product-price">$${price}</h3>
                            <a href="#!" data-productId="${id}" class="add-to-cart"><ion-icon name="cart-outline"></ion-icon></a>
                        </div>
                    </div>
                `;

                fragment.appendChild(productDiv);
            });

            productsContainer.appendChild(fragment);
            currentPage++;  
        } catch (error) {
            console.error('Error fetching products:', error);
        } finally {
            isLoading = false;
             loader.style.display = 'none';
        }
    }

   async function lazyLoadProducts() {
        const windowHeight = window.innerHeight;
        const documentHeight = document.body.offsetHeight;
        const scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;

        
        if (documentHeight - (scrollTop + windowHeight) <100 && !isLoading) {
            loader.style.display = 'block';
            await fetchAndDisplayProducts('https://fakestoreapi.com/products');
            loader.style.display = 'none';
        }
    }


    window.addEventListener('scroll', lazyLoadProducts);

    fetchAndDisplayProducts('https://fakestoreapi.com/products');
});