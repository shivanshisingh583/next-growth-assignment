// PAGE1- JS


    function displayOrderModal() {
        $('#orderModal').modal('show');
    }

    function clearFormFields() {
        document.getElementById('orderForm').reset();
    }
function submitOrder(event) {

    const form = document.getElementById('orderForm');
    if (form.checkValidity() === false) {
                    event.preventDefault();

        event.stopPropagation();
        form.classList.add('was-validated');
        return;
    }

  
    const formData = new FormData(form);

    const data = {};
    for (const [key, value] of formData) {
        data[key] = value;
    }

    const externalForm = document.createElement('form');
    externalForm.action = 'https://forms.maakeetoo.com/formapi/716';
    externalForm.method = 'POST';

    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            const inputField = document.createElement('input');
            inputField.type = 'hidden';
            inputField.name = key;
            inputField.value = data[key];
            externalForm.appendChild(inputField);
        }
    }

    document.body.appendChild(externalForm);
    externalForm.submit();

    $('#orderModal').modal('hide');
    clearFormFields();
        alert('Form details submitted successfully!');

}
    $('#orderModal').on('hidden.bs.modal', function () {
        clearFormFields();
    });

    $('.btn-outline-primary').click(function () {
        displayOrderModal();
    });
    $('.btn-primary').click(function () {
        displayOrderModal();
    });

const userRange = document.getElementById('userRange');
const selectedUsers = document.getElementById('selectedUsers');
const plans = [
    { element: document.getElementById('freePlan'), minUsers: 0, maxUsers: 10 },
    { element: document.getElementById('proPlan'), minUsers: 11, maxUsers: 20 },
    { element: document.getElementById('enterprisePlan'), minUsers: 21, maxUsers: 30 }
];

userRange.addEventListener('input', () => {
    const userCount = parseInt(userRange.value);
    selectedUsers.innerText = `${userCount} Users`;

    plans.forEach(plan => {
        const { element, minUsers, maxUsers } = plan;
        element.classList.toggle('highlighted-plan', userCount >= minUsers && userCount <= maxUsers);
    });
});







